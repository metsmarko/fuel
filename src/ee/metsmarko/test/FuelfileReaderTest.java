package ee.metsmarko.test;

import ee.metsmarko.app.Exception.InvalidDataException;
import ee.metsmarko.app.Exception.MalformedFileException;
import ee.metsmarko.app.Fueldata.FuelfileReader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.text.ParseException;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Marko Mets on 02.02.2015.
 */
public class FuelfileReaderTest {


    private static final String FILENAME = "fuelTest.txt";
    private FuelfileReader reader;
    private File file;

    @Before
    public void createMockFile() {
        reader = new FuelfileReader(FILENAME);
        try {
            file = new File(FILENAME);
            file.createNewFile();
        } catch (IOException e) {
        }
    }

    @After
    public void deleteMockFile() {
        file.delete();
    }

    @Test
    public void correctFileTest() throws MalformedFileException, InvalidDataException, ParseException, IOException {
        writeCorrectDataToMockFile();
        assertEquals(4, reader.read().size());
    }

    @Test(expected = InvalidDataException.class)
    public void invalidValuesTest() throws MalformedFileException, InvalidDataException, ParseException, IOException {
        writeInvalidDataToMockFile();
        reader.read();
    }

    @Test(expected = MalformedFileException.class)
    public void malformedFileTest() throws MalformedFileException, InvalidDataException, ParseException, IOException {
        writeMalformedDataToMockFile();
        reader.read();
    }

    private void writeMalformedDataToMockFile() {
        PrintWriter writer = getWriter();
        writer.write("98|1.319|50.56|01.01.2014\n");
        writer.write("98|1.319|45,32|15.01.2014\n");
        writer.write("95|1.21|30.4|02.02.2014\n");
        writer.write("98|1.319|50.30\n");
        writer.flush();
        writer.close();
    }

    private void writeInvalidDataToMockFile() {
        PrintWriter writer = getWriter();
        writer.write("98|1.319|-50.56|01.01.2014\n");
        writer.write("98|1.319|45,32|15.01.2014\n");
        writer.write("95|1.21|30.4|02.02.2014\n");
        writer.write("98|1.319|50.30|09.02.2014\n");
        writer.flush();
        writer.close();
    }

    private void writeCorrectDataToMockFile() {
        PrintWriter writer = getWriter();
        writer.write("98|1.319|50.56|01.01.2014\n");
        writer.write("98|1.319|45,32|15.01.2014\n");
        writer.write("95|1.21|30.4|02.02.2014\n");
        writer.write("98|1.319|50.30|09.02.2014\n");
        writer.flush();
        writer.close();
    }

    private PrintWriter getWriter() {
        try {
            return new PrintWriter(FILENAME, "UTF-8");
        } catch (IOException e) {
        }
        return null;
    }


}
