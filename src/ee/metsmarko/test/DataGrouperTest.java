package ee.metsmarko.test;

import ee.metsmarko.app.Entity.Fuel;
import ee.metsmarko.app.Entity.Refuel;
import ee.metsmarko.app.Exception.InvalidDataException;
import ee.metsmarko.app.Exception.MalformedFileException;
import ee.metsmarko.app.Fueldata.DataGrouper;
import ee.metsmarko.app.Fueldata.FuelfileReader;
import org.junit.*;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Marko Mets on 02.02.2015.
 */
public class DataGrouperTest {

    private static final String FILENAME = "fuelTest.txt";
    private FuelfileReader reader;
    private File file;
    private List<Refuel> result;
    private DataGrouper grouper;

    @Before
    public void createMockFile() {
        reader = new FuelfileReader(FILENAME);
        grouper = new DataGrouper();
        writeCorrectDataToMockFile();
        try {
            result = reader.read();
            file = new File(FILENAME);
            file.createNewFile();
        } catch (IOException e) {
        } catch (MalformedFileException e) {
        } catch (InvalidDataException e) {
        } catch (ParseException e) {
        }
    }

    @After
    public void deleteMockFile() {
        file.delete();
    }

    @Test
    public void filterByNonExistentFuel(){
        HashMap<Integer, HashMap<String, Fuel>> groupedData = grouper.groupFuel(result, "D");
        int i = 0;
        for (HashMap<String, Fuel> h : groupedData.values()){
            i += h.size();
        }
        assertEquals(0, i);
    }

    @Test
    public void filterByExistentFuel(){
        HashMap<Integer, HashMap<String, Fuel>> groupedData = grouper.groupFuel(result, "98");
        int i = 0;
        for (HashMap<String, Fuel> h : groupedData.values()){
            i += h.size();
        }
        assertEquals(2, i);
    }

    private void writeCorrectDataToMockFile() {
        PrintWriter writer = getWriter();
        writer.write("98|1.319|50.56|01.01.2014\n");
        writer.write("98|1.319|45,32|15.02.2014\n");
        writer.write("95|1.21|30.4|02.02.2014\n");
        writer.write("98|1.319|50.30|09.02.2014\n");
        writer.flush();
        writer.close();
    }

    private PrintWriter getWriter() {
        try {
            return new PrintWriter(FILENAME, "UTF-8");
        } catch (IOException e) {
        }
        return null;
    }

}
