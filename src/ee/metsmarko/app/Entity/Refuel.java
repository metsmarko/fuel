package ee.metsmarko.app.Entity;

import java.util.Date;

/**
 * Class represents a refuel.
 */
public class Refuel {

    private Fuel fuel;

    private Date date;

    public Refuel(Fuel fuel, Date date){
        this.fuel = fuel;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public Fuel getFuel() {
        return fuel;
    }

}
