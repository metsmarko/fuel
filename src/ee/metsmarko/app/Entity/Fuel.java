package ee.metsmarko.app.Entity;

/**
 * Class that represents a fuel.
 */
public class Fuel {

    private String fuelName;
    private double fuelPrice, fuelAmount, totalPrice;

    public Fuel(String fuelName, double fuelPrice, double fuelAmount){
        this.fuelName = fuelName;
        this.fuelPrice = fuelPrice;
        this.fuelAmount = fuelAmount;
    }

    public Fuel(double totalPrice, double fuelAmount){
        this.totalPrice = totalPrice;
        this.fuelAmount = fuelAmount;
    }

    public String getFuelName() {
        return fuelName;
    }

    public double getFuelPrice() {
        return fuelPrice;
    }

    public double getFuelAmount() {
        return fuelAmount;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setFuelPrice(double fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public void setFuelAmount(double fuelAmount) {
        this.fuelAmount = fuelAmount;
    }
}
