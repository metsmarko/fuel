package ee.metsmarko.app.Fueldata;

import ee.metsmarko.app.Entity.Fuel;
import ee.metsmarko.app.Entity.Refuel;
import ee.metsmarko.app.Exception.InvalidDataException;
import ee.metsmarko.app.Exception.MalformedFileException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Class for reading the fuel data.
 */
public class FuelfileReader {

    private String fileName;

    private static final String SEPARATOR = "\\|";
    private BufferedReader reader;

    public FuelfileReader(String fileName){
        this.fileName = fileName;
    }

    /**
     * Reads fuel data from file.
     * @return List of all refuels.
     * @throws IOException
     * @throws MalformedFileException If file has missing information.
     * @throws ParseException
     * @throws InvalidDataException If values are negative.
     */
    public List<Refuel> read() throws IOException, MalformedFileException, ParseException, InvalidDataException {
        reader = getReader();
        List<Refuel> result = new ArrayList<Refuel>();
        String line;
        while((line = reader.readLine()) != null){
            String[] row = line.split(SEPARATOR);
            result.add(parseRow(row));
        }
        closeReader();
        return result;
    }

    private void closeReader() {
        try {
            reader.close();
        } catch (IOException e) {
        }
    }

    /**
     * Gets the last time that the file was modified.
     * @return Modification date.
     */
    public long getLastModificationDate(){
        return new File(fileName).lastModified();

    }

    private Refuel parseRow(String[] row) throws MalformedFileException, ParseException, InvalidDataException {
        if(row.length < 4){
            closeReader();
            throw new MalformedFileException("File is malformed and could not be read.");
        }
        String fuelName = row[0];
        double fuelPrice = Double.parseDouble(row[1].replaceAll(",", "."));
        double fuelAmount = Double.parseDouble(row[2].replaceAll(",", "."));
        validatePositive(fuelPrice);
        validatePositive(fuelAmount);
        Date date = parseDate(row[3]);
        Fuel fuel = new Fuel(fuelName, fuelPrice, fuelAmount);
        Refuel refuel = new Refuel(fuel, date);
        return refuel;
    }

    private void validatePositive(double d) throws InvalidDataException {
        if(d < 0){
            closeReader();
            throw new InvalidDataException("Fuel amount or price is negative");
        }
    }

    private Date parseDate(String s) throws ParseException {
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
        return df.parse(s);
    }

    private BufferedReader getReader() throws FileNotFoundException {
        return new BufferedReader(new FileReader(fileName));
    }

}
