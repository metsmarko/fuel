package ee.metsmarko.app.Fueldata;

import ee.metsmarko.app.Entity.Fuel;
import ee.metsmarko.app.Entity.Refuel;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Groups fuel data.
 */
public class DataGrouper {

    /**
     * Add all months to the hash map.
     *
     * @param result hash map.
     */
    private void addMonths(HashMap<Integer, HashMap<String, Fuel>> result) {
        for (int i = 0; i < 12; i++) {
            result.put(i, new HashMap<String, Fuel>());
        }
    }

    /**
     * Groups fuel by month and fuel type.
     * @param refuelData list of all refuels.
     * @param type Type of fuel to be included in the result.
     * @return Hash map containing data grouped my month and fuel type.
     */
    public HashMap<Integer, HashMap<String, Fuel>> groupFuel(List<Refuel> refuelData, String type) {
        HashMap<Integer, HashMap<String, Fuel>> result = new HashMap<Integer, HashMap<String, Fuel>>();
        addMonths(result);
        Calendar c = Calendar.getInstance();
        for (Refuel r : refuelData){
            c.setTime(r.getDate());
            int month = c.get(Calendar.MONTH);
            addFuelData(result.get(month), r, type);
        }
        return result;
    }

    /**
     * Calculates amount and total price of the fuel and adds it to hash map.
     * @param fuelHashMap Hash map where the fuel data is added.
     * @param r Refuel data.
     * @param type Type of the fuel.
     */
    private void addFuelData(HashMap<String, Fuel> fuelHashMap, Refuel r, String type) {
        String fuelType = r.getFuel().getFuelName();
        if(!type.equals("All") && !type.equals(fuelType)){
            return;
        }
        double fuelAmount = r.getFuel().getFuelAmount();
        double fuelPrice = r.getFuel().getFuelPrice();
        if(fuelHashMap.containsKey(fuelType)){
            Fuel f = fuelHashMap.get(fuelType);
            f.setFuelAmount(f.getFuelAmount() + fuelAmount);
            f.setTotalPrice(f.getTotalPrice() + (fuelPrice * fuelAmount));
        } else {
            Fuel f = new Fuel(fuelPrice*fuelAmount, fuelAmount);
            fuelHashMap.put(fuelType, f);
        }
    }
}
