package ee.metsmarko.app.Fueldata;

import ee.metsmarko.app.Entity.Refuel;
import ee.metsmarko.app.Exception.InvalidDataException;
import ee.metsmarko.app.Exception.MalformedFileException;
import ee.metsmarko.app.GUI.Frame;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * Parses fuel file.
 */
public class FileParser {

    private Frame frame;
    private long lastModified;
    private FuelfileReader reader;

    public FileParser(String fileName, Frame frame) {
        this.frame = frame;
        this.reader = new FuelfileReader(fileName);
        loadFile();
        watchFile();
    }

    /**
     * Watches for changes in the file and updates UI if file was changed.
     */
    private void watchFile() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Long modified = reader.getLastModificationDate();
                    if (reader.getLastModificationDate() != lastModified) {
                        lastModified = modified;
                        loadFile();
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {

                    }
                }
            }
        };
        r.run();
    }

    private void loadFile() {
        this.lastModified = reader.getLastModificationDate();
        try {
            List<Refuel> result = reader.read();
            frame.loadData(result);
        } catch (IOException e) {
            frame.showError(e.getMessage());
        } catch (MalformedFileException e) {
            frame.showError(e.getMessage());
        } catch (ParseException e) {
            frame.showError(e.getMessage());
        } catch (InvalidDataException e) {
            frame.showError(e.getMessage());
        }
    }

}
