package ee.metsmarko.app;

import ee.metsmarko.app.Fueldata.FileParser;
import ee.metsmarko.app.GUI.Frame;


public class Main {

    public static void main(String[] args) {
        Frame f = new Frame();
        if(args.length > 0 ){
            new FileParser(args[0], f);
        } else {
            f.showError("No file given");
        }
    }
}
