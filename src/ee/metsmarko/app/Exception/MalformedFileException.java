package ee.metsmarko.app.Exception;

/**
 * Signals that given file is malformed.
 */
public class MalformedFileException extends Exception {

    public MalformedFileException(String message){
        super(message);
    }

}
