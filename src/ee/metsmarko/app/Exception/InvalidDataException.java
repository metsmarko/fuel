package ee.metsmarko.app.Exception;

/**
 * Signals that invalid data was supplied.
 */
public class InvalidDataException extends Exception {

    public InvalidDataException(String message){
        super(message);
    }

}
