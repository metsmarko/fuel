package ee.metsmarko.app.GUI;

import ee.metsmarko.app.Entity.Fuel;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.HashMap;

/**
 * JPanel that holds the chart.
 */
public class BarChart extends JPanel {

    private HashMap<Integer, HashMap<String, Fuel>> data;

    String[] monthNames = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    private static final int OFFSET_BIG = 25;
    private static final int OFFSET_MEDIUM = 15;
    private static final int OFFSET_SMALL = 5;

    public BarChart(HashMap<Integer, HashMap<String, Fuel>> data, Dimension dimension) {
        super.setPreferredSize(dimension);
        super.setBackground(Color.WHITE);
        this.data = data;
    }

    // http://helpdesk.objects.com.au/java/how-to-display-bar-chart-using-swing
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        double max = findBiggestBarValue(data.values());
        double min = findSmallestBarValue(data.values());
        int width = (super.getWidth() / calcNumberOfBars(data.values()));

        int x = 0;

        for(Integer i : data.keySet()){
            HashMap<String, Fuel> type = data.get(i);
            for(String s : type.keySet()){
                Fuel f = type.get(s);
                double amount = f.getFuelAmount();
                double price = f.getTotalPrice();
                int height = (int) (getHeight() * (price / max));
                chooseColor(g, max, min, price);
                drawData(g, monthNames[i] + "-" + String.format("%.3f", amount)+"-"+s, height, width, x);
                x += width;
            }
            x+=2;
        }

        g.setColor(Color.BLACK);
        g.drawString(String.format("%.3f", max), 0, 10);
        g.drawString(String.format("%.3f", max/2), 0, super.getHeight()/2);
    }

    private void chooseColor(Graphics g, double max, double min, double val) {
        if(Double.compare(max, val) == 0){
            g.setColor(Color.RED);
        } else if(Double.compare(min, val) == 0){
            g.setColor(Color.GREEN);
        } else {
            g.setColor(Color.YELLOW);
        }
    }

    private void drawData(Graphics g, String val, int height, int width, int x) {
        g.fillRect(x, getHeight() - height, width, height);
        g.setColor(Color.BLACK);
        g.setFont(new Font("TimesRoman", Font.PLAIN, 12));
        g.drawString(val.split("-")[0], x, getHeight() - OFFSET_BIG);
        g.drawString(val.split("-")[1], x, getHeight() - OFFSET_MEDIUM);
        g.drawString(val.split("-")[2], x, getHeight() - OFFSET_SMALL);
    }

    private int calcNumberOfBars(Collection<HashMap<String, Fuel>> data) {
        int count = 0;
        for (HashMap<String, Fuel> h : data){
            count += h.size();
        }
        return count;
    }

    private double findBiggestBarValue(Collection<HashMap<String, Fuel>> data) {
        Double max = 0.0;
        for (HashMap<String, Fuel> h : data){
            for(String s : h.keySet()){
                Fuel f = h.get(s);
                max = Math.max(f.getTotalPrice(), max);
            }
        }
        return max;
    }

    private double findSmallestBarValue(Collection<HashMap<String, Fuel>> data) {
        Double min = Double.MAX_VALUE;
        for (HashMap<String, Fuel> h : data){
            for(String s : h.keySet()){
                Fuel f = h.get(s);
                min = Math.min(f.getTotalPrice(), min);
            }
        }
        return min;
    }

}
