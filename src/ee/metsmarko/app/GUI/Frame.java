package ee.metsmarko.app.GUI;

import ee.metsmarko.app.Entity.Fuel;
import ee.metsmarko.app.Entity.Refuel;
import ee.metsmarko.app.Fueldata.DataGrouper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

/**
 * Main window.
 */
public class Frame extends JFrame {

    private List<Refuel> refuelData;
    private JPanel panel;
    private Dimension panelDimension = new Dimension(1480, 400);

    public Frame() {
        setup();
        super.setVisible(true);
    }

    private void setup() {
        super.setPreferredSize(new Dimension(1500, 500));
        super.setResizable(false);
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        super.setTitle("Refuel Data");
        super.setLayout(new FlowLayout());
        super.getContentPane().setBackground(Color.LIGHT_GRAY);
        super.pack();
    }

    /**
     * Loads refuel data and draws bar charts.
     *
     * @param refuelData data to be loaded.
     */
    public void loadData(List<Refuel> refuelData) {
        this.refuelData = refuelData;
        super.getContentPane().removeAll();
        makeComboBox();
        panel = new JPanel();
        panel.setPreferredSize(panelDimension);
        super.getContentPane().add(panel);
        super.pack();
        displayData("All");
    }

    private void makeComboBox() {
        List<String> fuelTypes = retrieveAllFuelTypes();
        final JComboBox comboBox = new JComboBox(fuelTypes.toArray());
        comboBox.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                displayData(comboBox.getSelectedItem().toString());
            }
        });
        addElement(comboBox);
    }

    private List<String> retrieveAllFuelTypes() {
        List<String> result = new ArrayList<String>();
        result.add("All");
        for (Refuel r : refuelData) {
            if(!result.contains(r.getFuel().getFuelName())){
                result.add(r.getFuel().getFuelName());
            }
        }
        return result;
    }

    private void displayData(String type) {
        DataGrouper dg = new DataGrouper();
        HashMap<Integer, HashMap<String, Fuel>> data = dg.groupFuel(refuelData, type);
        panel.removeAll();
        panel.add(new BarChart(data, panelDimension));
        super.pack();
        super.repaint();
    }

    /**
     * Shows an error in a Text area.
     *
     * @param message error message.
     */
    public void showError(String message) {
        JTextArea ta = createTextArea("Error: " + message);
        super.getContentPane().removeAll();
        addElement(ta);
    }

    private JTextArea createTextArea(String message) {
        JTextArea ta = new JTextArea(message);
        ta.setEditable(false);
        ta.setVisible(true);
        ta.setEnabled(true);
        ta.setPreferredSize(new Dimension(500, 50));
        return ta;
    }

    private void addElement(Component c){
        super.getContentPane().add(c);
        super.pack();
        super.repaint();
    }
}
